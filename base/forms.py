from django import forms
from django.contrib.auth.models import User


from base.models import Internos


class InternForm(forms.ModelForm):


    class Meta:
        model = Internos
        fields = ['nombre','ape1','ape2','edad','sexo','direccion','telefono','foto'] 
