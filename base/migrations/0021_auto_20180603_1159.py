# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-06-03 16:59
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0020_auto_20180603_1147'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ingreso',
            old_name='user',
            new_name='usuario',
        ),
        migrations.RenameField(
            model_name='internos',
            old_name='user',
            new_name='usuario',
        ),
    ]
