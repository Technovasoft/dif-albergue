# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-08-03 11:24
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0036_auto_20180624_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='internos',
            name='codigo',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True, max_length=16),
        ),
    ]
