from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from almacen.models import Insumos
import uuid
# Create your models here.

class Profile(models.Model):
    User_Type_Choices = (
            (1,'dependiente'),
            (2,'psicologo'),
            (3,'medico'),
            (4,'promotor'),
            (5,'supervisor'),
            (6,'bodega'),
            (7,'admin'))

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_type = models.PositiveSmallIntegerField(choices=User_Type_Choices,blank=True)
    user_image = models.FileField(blank=True)


    def __str__(self):
        return self.user.username

class Camas(models.Model):
    Nhab        =    models.CharField(max_length=30)
    cama        =    models.CharField(max_length=20)
    usada       =    models.BooleanField(default=False)

    def __str__(self):
        return f'H: {self.Nhab}-C:{self.cama}'

class InsumosPrestados(models.Model):
    ingreso      =   models.IntegerField()
    nombre       =   models.CharField(max_length=100)
    cantidad     =   models.IntegerField(default=1)

    def __str__(self):
        return self.nombre

class Internos(models.Model):
    #fields for the creations
    user         =   models.IntegerField(default=1)
    codigo       =   models.UUIDField(max_length=16, default=uuid.uuid4, editable=False,unique=True)
    nombre       =   models.CharField(max_length=50) 
    ape1         =   models.CharField(max_length=50)
    ape2         =   models.CharField(max_length=50)
    edad         =   models.IntegerField(default=1)
    sexo         =   models.CharField(max_length=30,default='Masculino')
    embarazo     =   models.BooleanField(default=False,blank= True)
    estado       =   models.IntegerField(default=1)
    municipio    =   models.IntegerField(default=1)
    direccion    =   models.CharField(max_length=100)
    telefono     =   models.CharField(max_length=20)
    foto         =   models.FileField()
    alta         =   models.BooleanField(default=False)
    indigena     =   models.BooleanField(default=False)
    reporte      =   models.BooleanField(default=False)
   
    def __str__(self):
        return self.nombre


class Ingreso(models.Model):
    Dietas_Choices = (
            (1,'blanda'),
            (2,'baja en grasa'),
            (3,'estandar'))
    #flieds for the ingreso
    user         =   models.IntegerField(default=1)
    interno      =   models.OneToOneField(Internos,default=None)
    especialidad =   models.CharField(max_length=200)
    hospitalizado=   models.BooleanField(default=False)
    diagnostico  =   models.CharField(max_length=150)
    dieta        =   models.PositiveSmallIntegerField(choices=Dietas_Choices)
    fechaIngre   =   models.DateField(auto_now_add=True)
    fechaCita    =   models.DateField()
    fechaExit    =   models.DateField(blank=True)
    cama         =   models.OneToOneField(Camas,blank=True)
    acompas      =   models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.interno.nombre

    
class InsumosPrestados(models.Model):
    ingreso      =   models.IntegerField()
    nombre       =   models.CharField(max_length=100)
    cantidad     =   models.IntegerField(default=1)

    def __str__(self):
        return self.nombre 
    
class Acompa(models.Model):
    nombre       =   models.CharField(max_length=50)
    ape1         =   models.CharField(max_length=50)
    ape2         =   models.CharField(max_length=50)
    edad         =   models.IntegerField(default=1)
    sexo         =   models.CharField(max_length=30,default='Masculino')
    foto         =   models.FileField()
    Paciente     =   models.IntegerField(default=0)
    cama         =   models.OneToOneField(Camas,default=None)

    def __str__(self):
        return self.nombre


class Reportes(models.Model):
    interno   =    models.IntegerField()
    n1        =    models.TextField(blank=True)
    n2        =    models.TextField(blank=True)
    n3        =    models.TextField(blank=True)

    def __str__(self):
        return self.interno

# Salidas 

class Salida(models.Model):
    Dietas_Choices = (
            (1,'blanda'),
            (2,'baja en grasa'),
            (3,'estandar'))
    #fields for the ingreso
    user         =   models.IntegerField(default=1)
    interno      =   models.IntegerField(default=1)
    especialidad =   models.CharField(max_length=60)
    hospitalizado=   models.BooleanField(default=False)
    diagnostico  =   models.CharField(max_length=150)
    dieta        =   models.PositiveSmallIntegerField(choices=Dietas_Choices)
    fechaSalida   =   models.DateField(auto_now_add=True)
    fechaCita    =   models.DateField()
    cama         =   models.IntegerField(default=1,blank=True)
    acompas      =   models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.especialidad

class Salidacompa(models.Model):
    nombre       =   models.CharField(max_length=50)
    ape1         =   models.CharField(max_length=50)
    ape2         =   models.CharField(max_length=50)
    edad         =   models.IntegerField(default=1)
    sexo         =   models.CharField(max_length=30,default='Masculino')
    foto         =   models.FileField()
    Paciente     =   models.IntegerField(default=0)
    cama         =   models.OneToOneField(Camas,default=None)

    def __str__(self):
        return self.nombre


class SalidaInsumos(models.Model):
    salida      =   models.IntegerField()
    nombre       =   models.CharField(max_length=100)
    cantidad     =   models.IntegerField(default=1)

    def __str__(self):
        return self.nombre



# especialidad
class Especialidades(models.Model):
    especialidades = models.CharField(max_length=200)
    def __str__(self):
        return self.especialidades

# Estados y Municipios

class Estados(models.Model):
    #field of states
    estado = models.CharField(max_length=40)

    def __str__(self):
        return self.estado

class Municipios(models.Model):
    #field of states
    estado    =   models.IntegerField(default=1)
    municipio =   models.CharField(max_length=150)

    def __str__(self):
        return self.municipio

