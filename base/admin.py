from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Profile,Camas,Internos,Acompa,Ingreso,Reportes
from .models import InsumosPrestados,Salida,Estados,Municipios,Especialidades
from .models import Salidacompa,SalidaInsumos,Especialidades
# Register your models here.

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'

class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )


    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)

class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'get_usertype')
    list_select_related = ('profile', )

    def get_usertype(self, instance):
        return instance.profile.user_type
    get_usertype.short_description = 'TIPO'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


class AdminCamas(admin.ModelAdmin):
    list_display = ['Nhab','cama','stock']

    def stock(self,instance):
        return instance.usada
    stock.short_description = "Usada"





admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Camas, AdminCamas)
admin.site.register(Internos)
admin.site.register(Acompa)
admin.site.register(Ingreso)
admin.site.register(Reportes)
admin.site.register(InsumosPrestados)
admin.site.register(Salida)
admin.site.register(Salidacompa)
admin.site.register(SalidaInsumos)
admin.site.register(Especialidades)

admin.site.register(Estados)
admin.site.register(Municipios)