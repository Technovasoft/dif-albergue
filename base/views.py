from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views import generic
from django.conf import settings
from django.views.generic.edit import FormView
from django.http import HttpResponse

#rest api
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import MunicipiosSerializer
# utils imports
from datetime import  timedelta
from openpyxl import Workbook

from easy_pdf.views import PDFTemplateView

from .models import Camas,Ingreso,Internos,Reportes,Profile,InsumosPrestados
from .models import Acompa,Salida,Estados,Municipios,Salidacompa,SalidaInsumos
from .models import Especialidades
from almacen.models import Insumos
from .forms import InternForm
from .utils import decode_base64_file

def index(request):
    #check your type of user.
    cama = Camas.objects.filter(usada=False)
    return render(request, 'base/index.html', {
        'message':"",
        'camas': cama,
    })

def ayuda(request):
    return render(request, 'base/ayuda.html', {})

def pacienteView(request):
    #check your type of user.
    Inter = Internos.objects.all()
    return render(request, 'base/pacientes.html', {
        'message':"",
        'pacientes': Inter,
    })

class PacienteDetail(generic.DetailView):
    model = Internos
    template_name = 'base/pacientesdetail.html'


    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PacienteDetail, self).get_context_data(**kwargs)
        report = Reportes.objects.filter(interno=self.object.id)
        user = Profile.objects.get(user=self.request.user)
        estado = Estados.objects.get(id=self.object.estado)
        municipio = Municipios.objects.get(id=self.object.municipio)
        if self.request.session.has_key('pi'):
            context['pi'] = True
            del self.request.session['pi']
        context['reportes'] = report
        context['user'] = user
        context['estado'] = estado
        context['municipio'] = municipio
        context['codigo'] = str(self.object.codigo).split("-")[0]

        return context
 


def personalView(request):
    #check your type of user.
    profiles = Profile.objects.get(user=request.user)
    if not profiles.user_type == 7:
        return redirect('/')
    user = Profile.objects.get(user=request.user)
    perso = Profile.objects.all()
    return render(request, 'base/personal.html', {
        'message':"",
        'personal': perso,
        'user': user
    })


class PersonalDetail(generic.DetailView):
    model = Profile
    template_name = 'base/personaldetail.html'


    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PersonalDetail, self).get_context_data(**kwargs)
        user = Profile.objects.get(user=self.request.user)
        context['user'] = user
        return context


def IngresadosView(request):
    #check your type of user.
    ingre = Ingreso.objects.all()
    return render(request, 'base/ingresados.html', {
        'message':"",
        'ingresados': ingre,
    })

class IngresadosDetail(generic.DetailView):
    model = Ingreso
    template_name = 'base/ingresosdetail.html'


    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(IngresadosDetail, self).get_context_data(**kwargs)
        user = Profile.objects.get(user=self.request.user)
        insumos = InsumosPrestados.objects.filter(ingreso=self.object.id)
        insumos01 = Insumos.objects.all()
        try:
            acompa = Acompa.objects.filter(Paciente=self.object.interno.id)
        except:
            acompa = None
        context['user'] = user
        context['insumosp'] = insumos
        context['insumos'] = insumos01
        context['codigo'] = str(self.object.interno.codigo).split('-')[0]
        if acompa :
            context['acompas'] = acompa
        return context
 



# forms views
def IngresoDrop(request):
    ingreso = Ingreso.objects.all()
    return render(request, 'base/ingreso_form_delete2.html', {
        'ingresos': ingreso,
        })


def IngresoDrop2(request):
    if request.method == 'POST':
        interno = request.POST['interno']
        print(interno)
        ingreso = Ingreso.objects.get(id = interno )
        insumos = InsumosPrestados.objects.filter(ingreso=ingreso.id)
    return render(request, 'base/ingreso_form_delete.html', {
        'ingreso': ingreso,
        'insumos': insumos,
        })


def IngresoDrop22(request,ingreso_id):
    print(ingreso_id)
    ingreso = Ingreso.objects.get(id = ingreso_id )
    insumos = InsumosPrestados.objects.filter(ingreso=ingreso_id)
    return render(request, 'base/ingreso_form_delete.html', {
        'ingreso': ingreso,
        'insumos': insumos,
        })


def IngresoDrop3(request):
    if request.method == 'POST':

        ingreso_id = request.POST['ingreso']         

        ingre = Ingreso.objects.get(id = ingreso_id)
        insumos = InsumosPrestados.objects.filter(ingreso=ingreso_id)
        interno = Internos.objects.get(id = ingre.interno.id)
        try:
            acompa = Acompa.objects.filter(Paciente=interno.id)
            cama = Camas.objects.get(id=ingre.cama.id)
        except:
            acompa = None
            cama = None

        interno.alta = False  
        interno.save()

        if cama == None:
            salida = Salida(user=request.user.id,interno=interno.id,especialidad=ingre.especialidad,
                    hospitalizado=ingre.hospitalizado,diagnostico=ingre.diagnostico,dieta=ingre.dieta,
                    fechaCita=ingre.fechaCita,acompas = ingre.acompas)
        else:
            salida = Salida(user=request.user.id,interno=interno.id,especialidad=ingre.especialidad,
                    hospitalizado=ingre.hospitalizado,diagnostico=ingre.diagnostico,dieta=ingre.dieta,
                    fechaCita=ingre.fechaCita,cama = cama.id,acompas = ingre.acompas)
            cama.usada = False
            cama.save()

        salida.save()

        for insu in insumos:
            a = Insumos.objects.get(name=insu.nombre)
            si = SalidaInsumos(salida=salida.id,nombre=insu.nombre,cantidad=1)
            a.prestadas -= 1
            a.save()
            si.save()

        insumos.delete()
        if(acompa):
            for acom in acompa:
                sa = Salidacompa(nombre=acom.nombre,ape1=acom.ape1,ape2=acom.ape2,edad=acom.edad,
                        sexo=acom.sexo,foto=acom.foto,Paciente=acom.Paciente,cama=cama)
                sa.save()
            acompa.delete()
        ingre.delete()

    return redirect('index')


def IngresoCreate(request):
    #check your type of user.

    internos = Internos.objects.filter(alta=False)
    camas = Camas.objects.filter(usada=False)
    insu  = Insumos.objects.all()
    espe  = Especialidades.objects.all()
    Dietas_Choices = (
        (1,'blanda'),
        (2,'baja en grasa'),
        (3,'estandar'))

    
    return render(request, 'base/ingreso_form.html', {
        'internos': internos,
        'camas': camas,
        'user': request.user,
        'insumos': insu,
        'especialidades': espe
    })

   
def ingresoForm(request):
    if request.method == 'POST':
        #form inputs
        interno=request.POST['interno']
        espe=request.POST['especialidad']
        diag=request.POST['diagnostico']
        dieta=request.POST['dieta']
        fechainit=request.POST['fechaini']
        #insumos
        insu=request.POST['insumo1']
        insum=request.POST['insumo2']
        insumo=request.POST['insumo3']
        insumoo=request.POST['insumo4']
        insumoss=request.POST['insumoS']

        Inter = Internos.objects.get(id=interno)
        if Inter.sexo == "femenino":
            if 'embarazada' in request.POST:
                Inter.embarazo = True
        Inter.alta= True
        Inter.save()


        if 'hospitalizado' in request.POST:
            ingre = Ingreso(user=request.user.id,interno=Inter,especialidad=espe,hospitalizado=True,
                    diagnostico=diag,dieta=dieta,fechaCita=fechainit,fechaExit=fechainit)
            ingre.save()
        else:
            if 'cama' in request.POST:
                cama=request.POST['cama']
                ca = Camas.objects.get(id=cama)
                ca.usada=True
                ca.save()
                ingre = Ingreso(user=request.user.id,interno=Inter,especialidad=espe,hospitalizado=False,
                        diagnostico=diag,dieta=dieta,fechaCita=fechainit,fechaExit=fechainit,cama=ca)
                ingre.save()


        #save the new states
        if insu != "-1":
            insumos = Insumos.objects.get(id=insu)
            insumos.prestadas +=1
            pres = InsumosPrestados(ingreso=ingre.id,nombre=insumos.name,cantidad=1) 

            insumos01 = Insumos.objects.get(id=insum)
            pres01 = InsumosPrestados(ingreso=ingre.id,nombre=insumos01.name,cantidad=1)
            insumos01.prestadas +=1

            insumos02 = Insumos.objects.get(id=insumo)
            pres02 = InsumosPrestados(ingreso=ingre.id,nombre=insumos02.name,cantidad=1)
            insumos02.prestadas +=1

            insumos03 = Insumos.objects.get(id=insumoo)
            pres03 = InsumosPrestados(ingreso=ingre.id,nombre=insumos03.name,cantidad=1)
            insumos03.prestadas +=1
            
            if insumoss != "-1":
                print(insumoss)
                insumos0S = Insumos.objects.get(id=insumoss)
                pres0S = InsumosPrestados(ingreso=ingre.id,nombre=insumos0S.name,cantidad=1)
                insumos0S.prestadas +=1
                insumos0S.save()
                pres0S.save()
            
            # Save
            insumos.save()
            insumos01.save()
            insumos02.save()
            insumos03.save()
            pres.save()
            pres01.save()
            pres02.save()
            pres03.save()

    if 'acompa' in request.POST:
        return redirect('base:add-acompa', ingre.id)
    else:
        return redirect('base:ingre-detail', ingre.id)

   



def addPacientes(request):
    estados = Estados.objects.all()
    municipios = Municipios.objects.all()
    return render(request, 'base/forma.html', {
            'message':"",
            'estados': estados,
        })

def addAcompa(request,ingreso_id):
    camas = Camas.objects.filter(usada=False)
    insu  = Insumos.objects.all()
    interno = Ingreso.objects.get(id=ingreso_id)
    return render(request, 'base/acompa_form.html', {
            'message':"",
            'camas':camas,
            'insumos': insu,
            'ingreso': interno,
        
        })



def addAcompaForm(request):
    if request.method == 'POST':
        # gettting the forms info
        interno=request.POST['interno']
        name=request.POST['nombre']
        ape1=request.POST['ape1']
        ape2=request.POST['ape2']
        edad=request.POST['edad']
        sexo=request.POST['sexo']
        base = request.POST['foto']
        cama=request.POST['cama']
         #insumos
        insu=request.POST['insumo1']
        insum=request.POST['insumo2']
        insumo=request.POST['insumo3']
        insumoo=request.POST['insumo4']
        insumoss=request.POST['insumoS']
        
        #instancia 
        ingre = Ingreso.objects.get(id=interno)
        cana = Camas.objects.get(id=cama)

        #acompañante creation 
        acompa = Acompa(nombre=name,ape1=ape1,ape2=ape2,edad=edad,sexo=sexo,
                foto=decode_base64_file(base),Paciente=ingre.interno.id,cama=cana)

        acompa.save()
        cana.usada = True
        cana.save()
        ingre.acompas +=1
        ingre.save()

        #save the new states
        insumos = Insumos.objects.get(id=insu)
        insumos.prestadas +=1
        pres = InsumosPrestados(ingreso=ingre.id,nombre=insumos.name,cantidad=1) 

        insumos01 = Insumos.objects.get(id=insum)
        pres01 = InsumosPrestados(ingreso=ingre.id,nombre=insumos01.name,cantidad=1)
        insumos01.prestadas +=1

        insumos02 = Insumos.objects.get(id=insumo)
        pres02 = InsumosPrestados(ingreso=ingre.id,nombre=insumos02.name,cantidad=1)
        insumos02.prestadas +=1

        insumos03 = Insumos.objects.get(id=insumoo)
        pres03 = InsumosPrestados(ingreso=ingre.id,nombre=insumos03.name,cantidad=1)
        insumos03.prestadas +=1
       
        if insumoss != "-1":
            print(insumoss)
            insumos0S = Insumos.objects.get(id=insumoss)
            pres0S = InsumosPrestados(ingreso=ingre.id,nombre=insumos0S.name,cantidad=1)
            insumos0S.prestadas +=1
            insumos0S.save()
            pres0S.save()
 
        
        insumos.save()
        insumos01.save()
        insumos02.save()
        insumos03.save()
        pres.save()
        pres01.save()
        pres02.save()
        pres03.save()

    return redirect('base:ingre-detail',interno) 


def add_reporte(request, interno_id):
    text = request.POST['reporte']
    tipo = request.POST['nivel']
    repo = Reportes(interno=interno_id)
    interno = Internos.objects.get(id=interno_id)
    interno.reporte=True
    interno.save()
    if tipo == "N1":
        repo.n1 = text
        repo.save()
    elif tipo == "N2":
        repo.n2 = text
        repo.save()
    else:
        repo.n3 = text
        repo.save()

    return redirect('base:paciente-detail', interno_id)

def add_insumos(request,ingreso_id):
    if request.method == 'POST':
        if 'insumo' in request.POST:
            insu = request.POST['insumo']
            insumo1 = Insumos.objects.get(id=insu)
            insumo1.prestadas += 1
            insumo1.save()
            aux = InsumosPrestados(ingreso=ingreso_id,nombre=insumo1.name,cantidad=1)
            aux.save()
        if 'insumo2' in request.POST:
            insumo = request.POST['insumo1']
            insumo2 = Insumos.objects.get(id=insumo)
            insumo2.prestadas += 1
            insumo2.save()
            aux2= InsumosPrestados(ingreso=ingreso_id,nombre=insumo2.name,cantidad=1)
            aux2.save()       

    return redirect('base:ingre-detail',ingreso_id)






#pdf class 
class PDFCamas(PDFTemplateView):
    template_name = 'base/camaspdf.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'camas_usadas.pdf'

    def get_context_data(self, **kwargs):
        cama = Camas.objects.filter(usada=True)
        return super(PDFCamas, self).get_context_data(
                    pagesize='A4',
                    title='hola',
                    camas=cama,
                    **kwargs
                )

class PDFComidas(PDFTemplateView):
    template_name = 'base/comidaspdf.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'tipos_de_comida.pdf'

    def get_context_data(self, **kwargs):

        comi = Ingreso.objects.all()
        ba=0
        baja=0
        estan=0
        for co in comi:
            if co.dieta == 1:
                ba+= 1
            elif co.dieta==2:
                baja+= 1
            else:
                estan+=1
        return super(PDFComidas, self).get_context_data(
                    pagesize='A4',
                    title='hola',
                    blanda=ba,
                    bajag=baja,
                    estandar=estan,
                    **kwargs
                )

class PDFReportes(PDFTemplateView):
    template_name = 'base/reportespdf.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Reportes_de usuarios.pdf'

    def get_context_data(self, **kwargs):
        repo = Reportes.objects.all()
        aux = Internos.objects.filter(reporte=True)
         
        return super(PDFReportes, self).get_context_data(
                    pagesize='A4',
                    title='hola',
                    reportes=repo,
                    usuarios=aux,
                    **kwargs
                )


#XLS class 
class XLSCamas(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        cama = Camas.objects.filter(usada=True)
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE CAMAS USADAS'

        ws.merge_cells('B1:E1')
        ws['B3']='NUMERO DE HABITACION'
        ws['C3']='CAMA'
        ws['D3']='HABITANTE'
        ws['E3']='ACCION'

        cont = 4
        for p in cama:
            ws.cell(row= cont,column=2).value = p.Nhab
            ws.cell(row= cont,column=3).value = p.cama
            try:
                a = Ingreso.objects.get(cama=p.id)
                ws.cell(row= cont,column=4).value = ''+a.interno.nombre+' '+a.interno.ape1+' '+a.interno.ape2
            except:
                a = Acompa.objects.get(cama=p.id)
                ws.cell(row= cont,column=4).value = ''+a.nombre+' '+a.ape1+' '+a.ape2


            cont+=1

        nombre_archivo = "ReporteCamas.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response

class XLSComidas(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        comi = Ingreso.objects.all()
        ba=0
        baja=0
        estan=0
        for co in comi:
            if co.dieta == 1:
                ba+= 1
            elif co.dieta==2:
                baja+= 1
            else:
                estan+=1
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE CAMAS USADAS'

        ws.merge_cells('B1:E1')
        ws['B3']='BLANDA' 
        ws['C3']='BAJA EN GRASA'
        ws['D3']='Estandar'
        cont = 4
        ws.cell(row= cont,column=2).value = ba
        ws.cell(row= cont,column=3).value = baja
        ws.cell(row= cont,column=4).value = estan

        nombre_archivo = "ReporteComidas.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response

class XLSReportes(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        repo = Reportes.objects.all()
        users = Internos.objects.filter(reporte=True)
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE PERSONAS CON REPORTES'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='REPORTES'
        ws.merge_cells('C3:k3')

        cont = 4
        ri = 3
        for p in users:
            repos = Reportes.objects.filter(interno=p.id)
            ws.cell(row= cont,column=2).value =''+p.nombre+' '+p.ape1+' '+p.ape2

            for r in repos:
                if(r.n1):
                    ws.cell(row= cont,column=ri).value = r.n1
                elif(r.n2):
                    ws.cell(row= cont,column=ri).value = r.n2
                elif(r.n3):
                    ws.cell(row= cont,column=ri).value = r.n3
                ri +=1
            cont+=1

        nombre_archivo = "Reportes.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response




#API VIEWS
class MunicipioList(APIView):
    def get(self,request,pk):
        aux = Municipios.objects.filter(estado=pk)
        serializer= MunicipiosSerializer(aux,many=True)
        return Response(serializer.data)
        





