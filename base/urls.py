from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

app_name='base'

urlpatterns = [
        # API URLS
        url(r'^municipios/(?P<pk>[0-9]+)$',views.MunicipioList.as_view()),
        #normalVistas
        url(r'^camas$', login_required(views.index, login_url='/'), name="index"),
        url(r'^pacientes$', login_required(views.pacienteView, login_url='/'), name="pacientes"),
        url(r'^personal$', login_required(views.personalView, login_url='/'), name="personal"),
        url(r'^personal/(?P<pk>[0-9]+)/$', login_required(views.PersonalDetail.as_view(), login_url='/'), name="personal-detail"),
        url(r'^paciente/(?P<pk>[0-9]+)/$', login_required(views.PacienteDetail.as_view(), login_url='/'), name="paciente-detail"),
        url(r'^Ingresados/$', login_required(views.IngresadosView, login_url='/'), name="ingresados"),
        url(r'^Ingresado/(?P<pk>[0-9]+)/$', login_required(views.IngresadosDetail.as_view(), login_url='/'), name="ingre-detail"),

        #forms
        url(r'^registro/pacientes$', login_required(views.addPacientes, login_url='/'), name="add-pacientes"),
        url(r'^registro/ingreso$', login_required(views.IngresoCreate, login_url='/'), name="add-ingreso"),
        url(r'^registro/acompa/(?P<ingreso_id>[0-9]+)$', login_required(views.addAcompa, login_url='/'), name="add-acompa"),
        url(r'^ingreso/form$', login_required(views.ingresoForm, login_url='/'), name="ingreso-form"),
        url(r'^ingreso/acompa$', login_required(views.addAcompaForm, login_url='/'), name="add-acompaform"),
        url(r'^reporte/(?P<interno_id>[0-9]+)$', login_required(views.add_reporte, login_url='/'), name="add-reporte"),
        url(r'^insuform/(?P<ingreso_id>[0-9]+)$', login_required(views.add_insumos, login_url='/'), name="add-insumos"),

        # delete forms 
        url(r'^eliminar/pacientes$', login_required(views.IngresoDrop, login_url='/'), name="drop-ingreso"),
        url(r'^eliminar/pacientes/paso2$', login_required(views.IngresoDrop2, login_url='/'), name="drop-ingreso2"),
        url(r'^eliminar/pacientes/(?P<ingreso_id>[0-9]+)$', login_required(views.IngresoDrop22, login_url='/'),name="drop-ingreso22"),
        url(r'^eliminar/pacientes/paso3$', login_required(views.IngresoDrop3, login_url='/'), name="drop-ingreso3"),

        #reportes variados
        url(r'^reporte/camas$',  login_required(views.PDFCamas.as_view(), login_url='/'), name="camaspdf"),
        url(r'^reporte/comidas$',  login_required(views.PDFComidas.as_view(), login_url='/'), name="comidaspdf"),
        url(r'^reporte/reportes$',  login_required(views.PDFReportes.as_view(), login_url='/'), name="reportespdf"),

        #reportes variados excel
        url(r'^reporte/camas/xls$',  login_required(views.XLSCamas.as_view(), login_url='/'), name="camasxls"),
        url(r'^reporte/comidas/xls$',  login_required(views.XLSComidas.as_view(), login_url='/'), name="comidasxls"),
        url(r'^reporte/reportes/xls$',  login_required(views.XLSReportes.as_view(), login_url='/'), name="reportesxls"),

        #Ayuda
        url(r'^ayuda$',  login_required(views.ayuda, login_url='/'), name="ayuda"),
    ]

