# DIF panel admnistrativo


## Requerimientos

Sistemas Operativos soportados:

* Entorno de Desarrollo: GNU/Linux, MacOS X, Windows.
* Entorno de Producción: GNU/Linux.

Requerimientos:

* virtualenv
* python == 3.6
* django == 1.11

## Obetener el código

    $ git clone https://gitlab.com/Technovasoft/dif-albergue.git
    $ cd dif-albergue

## Correr el código

Lo primero que tienes que hacer  una vez tienes el repositorio clonado es crear un entorno vitrual

    $ vitualenv dif
    $ cd dif

Despues de crear el entorno vitual toca activar este entorno

    $ source bin/activate

Una vez con el entorno activado procederemos a instalar los requerimientos necesarias para correr la app      
para hacer esto  nos movemos a la carpeta del repositorio.

    $ cd .. && cd dif-albergue
    $ pip install -r requirements.txt

Despues de instalar los requerimientos de la app lo unico que tienes que hacer es correr el servidor

    $ python manage.py runserver

Una vez el servidor de prueba este corriendo debes de entrar en el puerto local en el cual suele correr django.

http://127.0.0.1:8000/


## Diseño

Móvil:<br>
<img src="https://i.imgur.com/PhjPETr.png" width="250"/>

## Licencia

El proyecto se encuentra bajo licencia Apache 2.0 - Ver [LICENCIA](LICENSE) para más detalles.<br>
La documentación y manuales se encuentra bajo Creative Commons [CC BY-ND 4.0](BY-ND.md) 