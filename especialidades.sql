INSERT INTO base_especialidades (especialidades) 
 VALUES ('Anatomía Patológica'),
		('Anestesiología'),
		('Cirugía General'),
		('Genética'),
		('Ginecología y Obstetricia'),
		('Medicina de Rehabilitación'),
		('Medicina Interna'),
		('Oftalmología'),
		('Ortopedia'),
		('Otorrinolaringología '),
		('Patología Clínica'),
		('Pediatría '),
		('Psiquiatría'),
		('Radiología e Imagen'),
		('Urgencias Médicas Quirúrgicas'),
		('Radioncología'),
		('Anestesiología Pediátrica'),
		('Angiología y Cirugía Vascular'),
		('Cardiología'),
		('Cirugía Cardiotorácica'),
		('Cirugía Oncológica'),
		('Cirugía Pediátrica'),
		('Cirugía Plástica y Reconstructiva'),
		('Coloproctologia'),
		('Dermatología'),
		('Endocrinología'),
		('Gastroenterología'),
		('Geriatría '),
		('Hematología'),
		('Infectologia'),
		('Infectologia Pediátrica '),
		('Medicina del Enfermo en Estado Critico '),
		('Nefrología '),
		('Neonatología'),
		('Neurocirugía'),
		('Oncología Médica'),
		('Reumatología '),
		('Urología'),
		('Urología Ginecológica '),
		('Alergia e Inmunologia Clinica Pediàtrica'),
		('Audiología, Foniatria y Otoneurología'),
		('Clinica de Mama'),
		('Clinica del Dolor'),
		('Endoscopia Diagnóstica y Terapéutica'),
		('Endoscopia Ginecológica'),
		('Estrabismo'),
		('Fisiología Obstétrica'),
		('Glaucoma'),
		('Oncología Ginecológica'),
		('Retina'),
		('Segmento Anterior'),
		('Urgencias Pediatría'),
		('Cirugía de Epilepsia'),
		('Cirugía Oncológica Pediátrica'),
		('Endoscopia Gastrointestinal Pediátrica'),
		('Atención Integral al Paciente con Infecciones de Transmisión Sexual diferentes a VIH');
