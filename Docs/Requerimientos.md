# Requerimientos Funcionales

* RF01 --> El sistema tendrá un sistema de usuarios donde existirá el usuario administrador el cual tendrá la
opción de poder modificar a la base de usuarios y hacer tareas administrativas del proprio sistema.
* RF02 --> El sistema tendrá un tipo de usuario supervisor que popdrá observar todos los procesos llevados acabo
por el sistema y sus usuarios pero no podrá participar en estos.
* RF03 --> El sistema tendrá un tipo de usuario "Dependiente" que tendrá la capacidad de: registrar usuarios 
entrantes, dar de alta y baja a estos mismo usuarios, entregar y requisar insumos, visualizar las incidencias.
* RF04 --> El sistema tendrá un tipo de usuario "Promotor" que tendrá la capacidad de: registrar usuarios 
entrantes, dar de alta y baja a estos mismo usuarios, entregar y requisar insumos, visualizar las incidencias y crearlas.
* RF05 --> El sistema tendrá un tipo de usuario "Médico" que tendrá la capacidad de: crear y modificar los historiales
médicos de los usuarios entrantes y hacer comentarios en ellos , visualizar las incidencias.
* RF06 --> El sistema tendrá un tipo de usuario "Psicólogo" que tendrá la capacidad de: revisar los historiales
médicos de los usuarios entrantes y hacer comentarios en ellos, visualizar las incidencias.
* RF07 --> El sistema tendrá un tipo de usuario "bodega" que tendra la capacidad de: revisar los items en la bodega,
agregar y eliminar existencia.
* RF08 --> El sistema tendra una bitacora de incidencias.
* RF09 --> El sistema contara con un sistema que permita ingresar insumos y prestarlos.
* RF10 --> El sistema contara con un directorio telefonico de mas albergues y sus administradores siempre
visible a todos los usuarios excepto bodega.
* RF11 --> El sistema contara con un calendario que sera accesible por todos los usuarios para observer las fechas
de cenas programadas.
* RF12 --> El sistema contara con un sistema de creacion de expedientes medicos los cuales podran ser comentados 
por los usuarios permitidos.
* RF13 --> El sistema tendra la habilidad de registrar las camas y las habitaciones del albergue en cuestion.
* RF14 --> El sistema contara con la habilidad de registrar enfermos y agregar las fechas de su citas entre otras
cosas especificadas en la documentacion del proyecto.
* RF15 --> El sistema contara con la habilidad de registrar los items de comida que se tienen en el albergue.
* RF16 --> El sistema contara con la habilidad de registrar las fechas de caducidad de los items de comida y 
dar alarmas sobre el estado de estos items.
* RF17 --> El sistema contara con la habilidad de registrar los items de medicina que se tienen en el albergue.
* RF18 --> El sistema contara con la habilidad de registrar las fechas de caducidad de los items de medicina 
y dar alarmas del estado de estos items.
* RF20 --> El sistema contara con la habilidad de registrar un solo acompañante del enfermo.
* RF21 --> El sistema tendrá la habilidad de poder comentar en los habitantes registrados en el albergue.
* RF22 --> El sistema tendra la habilidad de revisar en todo momento si un habitante tiene una fecha pasada.
* RF23 --> El sistema contara con la habilidad de generar reportes de las camas y sus habitantes en pdf.
* RF24 --> El sistema contara con la habilidad de generar reportes del numero de pacientes con sus diferentes
tipos de dieta especificados con la información obtenida en la minuta.
* RF25 --> El sistema contara con la habilidad de generar reportes de gente ingresada que tiene reporte de conducta
y que esta baneada y por cuanto tiempo en pdf.




# Requerimientos No Funcionales

* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
* RNF01 -->
