"""albergue URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls import handler403, handler404, handler500
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.auth.views import LoginView 
from . import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    #logs
    url(r'^login/', LoginView.as_view(template_name= 'login/index.html'), name='login'),
    url(r'^logout/', views.logout_view,name='logout'),
    #sistemas
    url(r'^panel/', login_required(views.panel.as_view(), login_url='login'), name='panel'),
    url(r'^almacen/',include('almacen.urls')),
    url(r'^',include('base.urls')),
    #indexform/test
    url(r'^form/', login_required(views.formindex, login_url='/'), name='form'),
    #perfil url
    url(r'^perfil/$', login_required(views.perfilView, login_url='/'), name="perfil"),
    #calendario/test
    url(r'^swingtime/events/type/([^/]+)/$', views.event_type, name='calen-event'),
    url(r'^swingtime/', include('swingtime.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Solo para produccion
handler403 = views.status_403
handler404 = views.status_404
handler500 = views.status_500



