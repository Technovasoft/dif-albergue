from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views import View
from base.models import Profile,Ingreso

from datetime import datetime, timedelta
from django.template.context import RequestContext

from base.models import Internos,Municipios
from base.utils import decode_base64_file
from swingtime import models as swingtime


def index(request):
    if request.user.is_authenticated:
        return redirect('/panel/')
    else:
        return redirect('/login/')

def logout_view(request):
    logout(request)
    return redirect('/')


class panel(View):
    def get(self,request):
        # Ejemplo en caso de presntarse algun error, se manda el error a la vista
        profiles = Profile.objects.get(user=request.user)    
        if profiles.user_type == 6:
            return redirect('almacen:index')
        return render(request, 'panel/index.html', {
            'message':'',
            'user': profiles
        })

        
def formindex(request):
    if request.method == 'POST':
        # gettting the forms info
        name=request.POST['nombre']
        ape1=request.POST['ape1']
        ape2=request.POST['ape2']
        edad=request.POST['edad']
        sexo=request.POST['sexo']
        direccion=request.POST['direccion']
        telefono=request.POST['telefono']
        estado=request.POST['estado']
        municipio=request.POST['municipio']
        indi = False
        if 'indigena' in request.POST:
            indi = True
        base = request.POST['foto']

        #create the intern object
        ingre = Internos(user=request.user.id,nombre=name,ape1=ape1,ape2=ape2,edad=edad,sexo=sexo,
                direccion=direccion,telefono=telefono,estado=estado,municipio=municipio,indigena=indi,foto=decode_base64_file(base))
        #saving the new intern
        ingre.save()
        request.session['pi'] = True
    return redirect('base:paciente-detail', ingre.id) 

        



def perfilView(request):
    Pro = Profile.objects.get(user=request.user)
    return render(request, 'base/perfil.html', {
        'message':"",
        'perfil': Pro,
    })




def status_403(request):
    return render(request, 'status/index.html', {
        'number':'403',
        'name':'Forbidden',
        'message':'Usted no tiene permiso para acceder a éste apartado.'
    })

def status_404(request):
    return render(request, 'status/index.html', {
        'number':'404',
        'name':'Not found',
        'message':'Verifique que la dirección en el navegador sea correcta.'
    })

def status_500(request):
    return render(request, 'status/index.html', {
        'number':'500',
        'name':'Internal server error',
        'message':'Ha ocurrido un error mientras se procesaba su solicitud, intente de nuevo más tarde.'
    })


#test views
def event_type(request, abbr):
    event_type = get_object_or_404(swingtime.EventType, abbr=abbr)
    now = datetime.now()
    occurrences = swingtime.Occurrence.objects.filter(
        event__event_type=event_type,
        start_time__gte=now,
        start_time__lte=now+timedelta(days=+30)
    )
    return render(request, 'base/upcoming_by_event_type.html', {
        'occurrences': occurrences,
        'event_type': event_type
        })



