$(function() {
    var navbar = true;

    $(window).resize(function(){
        if ($(window).width() <= 768){	
            $( ".sidebar" ).css( "width", "0%" );
        }else{
            $( ".sidebar" ).css( "width", "18%" );
        }
    });

    $( "#btn-navbar" ).click(function() {
        if(navbar){
            $( ".sidebar" ).css( "width", "70%" );
            navbar = false;
        }else{
            $( ".sidebar" ).css( "width", "0%" );
            navbar = true;
        }
    });
});