from django.db import models

# Create your models here.
class Productos(models.Model):
    name = models.CharField(max_length=400)
    cantidad = models.IntegerField(default=1)
    refrigerado= models.BooleanField(default=False)
    fechaI = models.DateField(auto_now_add=True)
    fechaC = models.DateField()
    almacen= models.CharField(max_length=100,default='test')
    especial = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Medicamentos(models.Model):
    name = models.CharField(max_length=400)
    cantidad = models.IntegerField(default=1)
    refrigerado= models.BooleanField(default=False)
    fechaI = models.DateField(auto_now_add=True)
    fechaC = models.DateField()

    def __str__(self):
        return self.name


class Insumos(models.Model):
    name = models.CharField(max_length=400)
    cantidad = models.IntegerField(default=1)
    prestadas = models.IntegerField(default=0)
    almacen= models.CharField(max_length=100 ,default='test')
    fechaI = models.DateField(auto_now_add=True)
    stock = models.BooleanField(default=False)
    especial = models.BooleanField(default=False)


    def __str__(self):
        return self.name


