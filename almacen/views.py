from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views import generic
from django.conf import settings
from django.http import HttpResponse

from easy_pdf.views import PDFTemplateView
from openpyxl import Workbook

from .models import Productos, Medicamentos, Insumos
from base.models import Profile,InsumosPrestados,Ingreso



#utils
import datetime

# Create your views here.

def index(request):
    #check your type of user.
    profiles = Profile.objects.get(user=request.user)
    if not profiles.user_type > 3 and profiles.user_type < 8:
        return redirect('/')

    pro = Productos.objects.all()
    #dinamycs dates
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(days=10)
    #search of the close dates
    pros = Productos.objects.filter(fechaC__range=(start_date, end_date))
    if pros != None:
        message = "Existen Productos a punto de caducar"
    else:
        message = "Que oso que seas almacenista"

    return render(request, 'almacen/index.html', {
        'message':message,
        'productos': pro,
        'prod':pros,
        'user': profiles
    })

def med(request):
    #check your type of user.
    profiles = Profile.objects.get(user=request.user)
    if not profiles.user_type > 3 and profiles.user_type < 8:
        return redirect('/')

    medi = Medicamentos.objects.all()
    #dinamycs dates
    start_date = datetime.date.today()
    end_date = start_date + datetime.timedelta(days=10)
    #search of the close dates
    medis = Medicamentos.objects.filter(fechaC__range=(start_date, end_date))
    if medis != None:
        message = "Existen Medicamentos a punto de caducar"
    else:
        message = "Que oso que seas almacenista"

    return render(request, 'almacen/med.html', {
        'message':message,
        'medicamentos': medi,
        'medc': medis,
        'user': profiles
    })

def insumos(request):
    #check your type of user.
    profiles = Profile.objects.get(user=request.user)
    if not profiles.user_type > 3 and profiles.user_type < 8:
        return redirect('/')

    insu = Insumos.objects.filter(especial=False)

    #insumos espaciales
    especiales = Insumos.objects.filter(especial=True)
    if especiales == None:
        message = "Existe un problema no hay insumos especiales"
    else:
        message = "Que oso que seas almacenista de insumos"

    return render(request, 'almacen/insumos.html', {
        'message':message,
        'insumos': insu,
        'espe': especiales,
        'user': profiles
    })

# vistas CREATE,DELETE,add-1,minus-1
class ProductosCreate(generic.CreateView):

    model = Productos
    success_url = '/almacen'
    fields = ['name','cantidad','refrigerado','fechaC','almacen','especial']
 
class MedicamentosCreate(generic.CreateView):

    model = Medicamentos
    success_url = '/almacen/medicamentos'
    fields = ['name','cantidad','refrigerado','fechaC']

def add_producto(request, producto_id):
    produ = Productos.objects.get(id=producto_id)
    produ.cantidad += 1
    produ.save()
    return redirect('almacen:index')

def minus_producto(request, producto_id):
    produ = Productos.objects.get(id=producto_id)
    produ.cantidad -= 1
    produ.save()
    return redirect('almacen:index')


def add_medicamento(request, medicamento_id):
    medi = Medicamentos.objects.get(id=medicamento_id)
    medi.cantidad += 1
    medi.save()
    return redirect('almacen:medicamentos')

def minus_medicamento(request, medicamento_id):
    medi = Medicamentos.objects.get(id=medicamento_id)
    medi.cantidad -= 1
    medi.save()
    return redirect('almacen:medicamentos')


#Reportes del almacen
class PDFProductosC(PDFTemplateView):
    template_name = 'almacen/test.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Productos_a_caducar.pdf'

    def get_context_data(self, **kwargs):
        #dinamycs dates
        
        start_date = datetime.date.today()
        past = start_date -datetime.timedelta(days=31)
        end_date = start_date + datetime.timedelta(days=10)
        #search of the close dates
        pros = Productos.objects.filter(fechaC__range=(start_date, end_date))
        cad = Productos.objects.filter(fechaC__range=(past, start_date))
        return super(PDFProductosC, self).get_context_data(
                    pagesize='A4',
                    title='Productos a caducar',
                    prod=pros,
                    cadu=cad,
                    **kwargs
                )


class PDFMedicamentosC(PDFTemplateView):
    template_name = 'almacen/testm.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Medicamentos_a_caducar.pdf'

    def get_context_data(self, **kwargs):
        #dinamycs dates
        start_date = datetime.date.today()
        past = start_date -datetime.timedelta(days=31)
        end_date = start_date + datetime.timedelta(days=10)
        #search of the close dates
        med = Medicamentos.objects.filter(fechaC__range=(start_date, end_date))
        cad = Medicamentos.objects.filter(fechaC__range=(past, start_date))
        return super(PDFMedicamentosC, self).get_context_data(
                    pagesize='A4',
                    title='Medicamentos a caducar',
                    medc=med,
                    cadu=cad,
                    **kwargs
                )


class PDFInsumosP(PDFTemplateView):
    template_name = 'almacen/testi.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Insumos_prestados.pdf'

    def get_context_data(self, **kwargs):
       #search of the close dates
        insu = InsumosPrestados.objects.all()
        return super(PDFInsumosP, self).get_context_data(
                    pagesize='A4',
                    title='Insumos prestados',
                    insup=insu,
                    **kwargs
                )



class PDFProductos(PDFTemplateView):
    template_name = 'almacen/productospdf.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Productos.pdf'

    def get_context_data(self, **kwargs):
        pros = Productos.objects.all()
        return super(PDFProductos, self).get_context_data(
                    pagesize='A4',
                    title='Productos',
                    prod=pros,
                    **kwargs
                )


class PDFMedicamentos(PDFTemplateView):
    template_name = 'almacen/medicamentospdf.html'

    base_url ='file://' + settings.STATIC_ROOT
    download_filename = 'Productos.pdf'

    def get_context_data(self, **kwargs):
        med = Medicamentos.objects.all()
        return super(PDFMedicamentos, self).get_context_data(
                    pagesize='A4',
                    title='Medicamentos',
                    medicamentos=med,
                    **kwargs
                )



#Reportes del almacen XLS
class XLSProductos(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        pros = Productos.objects.all()
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE PRODUCTOS'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='CANTIDAD'
        ws['D3']='REFRIGERADO'
        ws['E3']='ALMACEN'
        ws['F3']='FECHA CADUCIDAD'

        cont = 5
        for p in pros:
            ws.cell(row= cont,column=2).value = p.name
            ws.cell(row= cont,column=3).value = p.cantidad
            if p.refrigerado == True:
                ws.cell(row= cont,column=4).value = 'Si'
            else:
                ws.cell(row= cont,column=4).value = 'No'
            ws.cell(row= cont,column=5).value = p.almacen
            ws.cell(row= cont,column=6).value = p.fechaC
            cont+=1
        nombre_archivo = "ReporteProductos.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response




class XLSMedicamentos(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        pros = Medicamentos.objects.all()
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE Medicamentos'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='CANTIDAD'
        ws['D3']='REFRIGERADO'
        ws['E3']='FECHA CADUCIDAD'

        cont = 4
        for p in pros:
            ws.cell(row= cont,column=2).value = p.name
            ws.cell(row= cont,column=3).value = p.cantidad
            if p.refrigerado == True:
                ws.cell(row= cont,column=4).value = 'Si'
            else:
                ws.cell(row= cont,column=4).value = 'No'
            ws.cell(row= cont,column=5).value = p.fechaC
            cont+=1

        nombre_archivo = "ReporteMedicamentos.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response


class XLSProductosC(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        start_date = datetime.date.today()
        past = start_date -datetime.timedelta(days=31)
        #search of the close dates
        cad = Productos.objects.filter(fechaC__range=(past, start_date))

        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE PRODUCTOS CADUCADOS'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='CANTIDAD'
        ws['D3']='FECHA CADUCIDAD'

        cont = 3
        for p in cad:
            ws.cell(row= cont,column=2).value = p.name
            ws.cell(row= cont,column=3).value = p.cantidad
            ws.cell(row= cont,column=4).value = p.fechaC
            cont+=1
        nombre_archivo = "ReporteProductosCaducados.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response

class XLSMedicamentosC(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        start_date = datetime.date.today()
        past = start_date -datetime.timedelta(days=31)
        #search of the close dates
        cad = Medicamentos.objects.filter(fechaC__range=(past, start_date))

        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE MEDICAMENTOS CADUCADOS'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='CANTIDAD'
        ws['D3']='FECHA CADUCIDAD'

        cont = 3
        for p in cad:
            ws.cell(row= cont,column=2).value = p.name
            ws.cell(row= cont,column=3).value = p.cantidad
            ws.cell(row= cont,column=4).value = p.fechaC
            cont+=1
        nombre_archivo = "ReporteMedicamentosCaducados.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response

    pass

class XLSInsumosP(generic.TemplateView):
    def get(self,request,*args,**kwargs):
        insu = InsumosPrestados.objects.all()
        wb = Workbook()
        ws = wb.active
        ws['B1'] = 'REPORTE DE INSUMOS PRESTADOS'

        ws.merge_cells('B1:E1')
        ws['B3']='NOMBRE'
        ws['C3']='CANTIDAD'

        cont = 2
        for p in insu:
            ws.cell(row= cont,column=2).value = p.nombre
            ws.cell(row= cont,column=3).value = p.cantidad
            cont+=1

        nombre_archivo = "Reporte_insumos_prestados.xlsx"
        response = HttpResponse(content_type="application/ms-excel")
        content = "attachment; filename = {0}".format(nombre_archivo)
        response['Content-Disposition'] = content
        wb.save(response)
        return response




