from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

app_name='almacen'

urlpatterns = [
        #almacen normal
        url(r'^$', login_required(views.index, login_url='/'), name="index"),
        url(r'^medicamentos/$', login_required(views.med, login_url='/'), name="medicamentos"),
        #Creat,Delete,add-1,minus-1
        url(r'^productos/create$', login_required(views.ProductosCreate.as_view(), login_url='/'), name="add-producto"),
        url(r'^medicamentos/create$', login_required(views.MedicamentosCreate.as_view(), login_url='/'), name="add-medicamento"),
        url(r'^addp/(?P<producto_id>[0-9]+)$', login_required(views.add_producto, login_url='/'), name="add-1"),
        url(r'^minusp/(?P<producto_id>[0-9]+)$', login_required(views.minus_producto, login_url='/'), name="minus-1"),
        url(r'^addm/(?P<medicamento_id>[0-9]+)$', login_required(views.add_medicamento, login_url='/'), name="addm-1"),
        url(r'^minusm/(?P<medicamento_id>[0-9]+)$', login_required(views.minus_medicamento, login_url='/'), name="minusm-1"),
 
        #insumos
        url(r'^insumos/$', login_required(views.insumos, login_url='/'), name="insumos"),
        #Reportes de diferentes cosas.
        url(r'^reporte/producto$',  login_required(views.PDFProductos.as_view(), login_url='/'), name="pro"),
        url(r'^reporte/medicamento$',  login_required(views.PDFMedicamentos.as_view(), login_url='/'), name="med"),
        url(r'^reporte/producto/caducado$',  login_required(views.PDFProductosC.as_view(), login_url='/'), name="pc"),
        url(r'^reporte/medicamento/caducado$',  login_required(views.PDFMedicamentosC.as_view(), login_url='/'), name="mc"),
        url(r'^reporte/insumos/prestados$',  login_required(views.PDFInsumosP.as_view(), login_url='/'), name="ip"),

        #Reportes de diferentes cosas Excel.
        url(r'^reporte/producto/xls$',  login_required(views.XLSProductos.as_view(), login_url='/'), name="proxls"),
        url(r'^reporte/medicamento/xls$',  login_required(views.XLSMedicamentos.as_view(), login_url='/'), name="medxls"),
        url(r'^reporte/producto/caducado/xls$',  login_required(views.XLSProductosC.as_view(), login_url='/'), name="pcxls"),
        url(r'^reporte/medicamento/caducado/xls$',  login_required(views.XLSMedicamentosC.as_view(), login_url='/'), name="mcxls"),
        url(r'^reporte/insumos/prestados/xls$',  login_required(views.XLSInsumosP.as_view(), login_url='/'), name="ipxls")
    ]

