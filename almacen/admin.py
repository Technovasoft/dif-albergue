from django.contrib import admin
from .models import Medicamentos, Productos, Insumos
# Register your models here.

class AdminProductos(admin.ModelAdmin):
    list_display = ['name','cantidad','Fecha_cad']

    def Fecha_cad(self,instance):
        return instance.fechaC
    Fecha_cad.short_description = "Caducidad"


class AdminMedicamentos(admin.ModelAdmin):
    list_display = ['name','cantidad','Fecha_cad']

    def Fecha_cad(self,instance):
        return instance.fechaC
    Fecha_cad.short_description = "Caducidad"

class AdminInsumos(admin.ModelAdmin):
    list_display = ['name','cantidad','stock']

    def stock(self,instance):
        return instance.stock
    stock.short_description = "Disponible"


admin.site.register(Medicamentos, AdminMedicamentos)
admin.site.register(Productos, AdminProductos)
admin.site.register(Insumos, AdminInsumos)
